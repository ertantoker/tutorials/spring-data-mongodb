package de.eretantoker.tutorials.customerservice.repository;

import de.eretantoker.tutorials.customerservice.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CustomerRepository extends MongoRepository<Customer, String> {
    Optional<Customer> findByFirstNameAndLastName(String firstName, String lastName);
}
